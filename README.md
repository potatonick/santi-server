# Servidor Santi
Santi es un gestor de tareas para pequeños y medianos grupos de trabajo. Compatible con plataformas Windows, Mac OS y Linux.

## Cómo compilar el proyecto
Dado que no proveemos de ningún fichero ejecutable puedes crearlo tú mismo. Es necesario tener instaladas las herramientas de desarrollo de Java JDK para poder llevar a cabo una compilación. El binario generado es un fichero .jar autocontenido con todas las dependencias necesarias para funcionar en cualquier máquina que tenga Java.

Siga las instrucciones a continuación para compilar el servidor:

1. Clona este proyecto. Puedes usar la línea de comandos o cualquier programa con interfaz visual. Para la línea de comandos ejecuta `git clone https://gitlab.com/potatonick/santi-server.git`
2. Compila el proyecto y genera un archivo .jar con todas las dependencias incluídas. Para ello dependiendo de la plataforma que uses debes ejecutar `gradlew.bat shadowJar` en Windows o `./gradlew shadowJar` en Linux.
3. El fichero .jar se habrá generado en el directorio build/libs.

Una vez compilado el proyecto puedes ir a la siguiente sección que te guía cómo usar el servidor.

## Instrucciones de uso
Para usar este servidor se requiere tener implementada una base de datos externa basada en MySQL o MariaDB. Para más información puede consultar la [documentación oficial de MySQL](https://dev.mysql.com/doc/?) o la [documentación oficial de MariaDB](https://mariadb.com/kb/en/library/documentation/).

Los siguientes pasos asumen que tienes un servidor configurado con un usuario que tiene permisos de creación, eliminación y alteración de tablas en una base de datos, puesto en marcha y es accesible:


### 1. Instalar Java
Este servidor está creado enteramente con el lenguaje de programación Java, por lo tanto necesitas tener instalado Java en el sistema para poder usar el servidor. Dependiendo de tu plataforma puede instalarlo desde diferentes vías. Para tomar una referencia acuda a la [página de descargas de Java](https://java.com/en/download/).

Asegurate de que tienes acceso al comando `java` desde la línea de comandos. Es posible que necesites modificar la variable de entorno `PATH` para ello.

### 2. Exportar variables de entorno
Exporta las variables de entorno `SANTI_HOST=servidor/base_datos` donde `servidor` es el nombre de equipo o IP del servidor y `base_datos` es el nombre que le has puesto a la  base de datos que has creado. Exporta `SANTI_USER=usuario` reemplazando `usuario` por el nombre del usuario de la base de datos y por último también exporta `SANTI_PASSWORD=password` cambiando `password` por la contraseña del anterior usuario.

#### Linux
Desde Linux se pueden exportar las variables de entorno de la siguiente manera:
```
export SANTI_HOST=servidor/base_datos
export SANTI_USER=usuario
export SANTI_PASSWORD=password
```

#### Windows
Las variables se pueden crear desde la herramienta de propiedades del sistema y en la sección de variables de entorno.

### 3. Ejecutar servidor
Una vez tenga descargada una copia del fichero .jar del servidor puede ejecutarlo desde la línea de comandos.

#### Linux
Abra una terminal y sitúese en el mismo directorio que el fichero .jar del servidor mediante el comando `cd`. Una vez situado puede ejecutar el servidor mediante `./java -jar com.gitlab.potatonick.santi-server-VERSION-all.jar` reemplazando `VERSION` por la versión que tenga descargada.

#### Windows
Abra una línea de comandos CMD y sitúese en el mismo directorio donde contiene el fichero .jar del servidor mediante el comando `cd`. Una vez situado puede ejecutar el servidor mediante el comando `java -jar com.gitlab.potatonick.santi-server-VERSION-all.jar` reemplazando `VERSION` por la versión que tenga descargada.`