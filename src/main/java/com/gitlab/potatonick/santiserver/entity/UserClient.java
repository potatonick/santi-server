/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.entity;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user_client")
public class UserClient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "user_name", length = 15, nullable = false, unique = true)
    private String userName = "anonymous";

    @Column(name = "real_name", length = 30, nullable = false)
    private String realName = "Anonymous";

    @Column(name = "password", length = 30, nullable = false)
    private String password = "";

    @Column(name = "role", nullable = false)
    private Role role = Role.USER;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_client_task",
            joinColumns = { @JoinColumn(name = "user_client_id") },
            inverseJoinColumns = { @JoinColumn(name = "task_id") }
    )
    private Set<Task> tasks = new HashSet<>();

    public UserClient() { }

    @Override
    public boolean equals(Object o) {
        if (o instanceof UserClient) {
            // There's no need to compare against tasks as it could cause recursive problems
            final UserClient userClient = (UserClient) o;
            return userClient.getUserName().equals(getUserName()) &&
                   userClient.getRealName().equals(getRealName()) &&
                   userClient.getPassword().equals(getPassword()) &&
                   userClient.getRole().equals(getRole());
        }

        return false;
    }

    @Override
    public int hashCode() {
        // ID attribute is not considered intentionally
        return Objects.hash(userName, realName, password, role);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Task> getTasks() {
        return Collections.unmodifiableSet(tasks);
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task) {
        this.tasks.add(task);
    }

    public enum Role {
        USER (0),
        ADMINISTRADOR (1);

        private final int role;

        Role(int role) {
            this.role = role;
        }

        public int toInteger() {
            return this.role;
        }

        public static Role getRole(int role) {
            switch(role) {
                case 0:
                    return USER;
                case 1:
                    return ADMINISTRADOR;
                default:
                    return ADMINISTRADOR;
            }
        }
    }

}
