/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "title", length = 15, nullable = false)
    private String title = "Untitled task";

    @Column(name = "description", length = 200)
    private String description = "";

    @Column(name = "state", nullable = false)
    private State state = State.PLANNED;

    @Column(name = "priority", nullable = false)
    private Priority priority = Priority.LOW;

    @Column(name = "start_date", nullable = false)
    @Temporal(value = TemporalType.DATE)
    private Date plannedStartDate = new Date();

    @Column(name = "end_date", nullable = false)
    @Temporal(value = TemporalType.DATE)
    private Date plannedEndDate = new Date();

    @ManyToMany(
            mappedBy = "tasks",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private Set<UserClient> userClients = new HashSet<>();

    public Task() {}

    @Override
    public boolean equals(Object o) {
        if (o instanceof Task) {
            final Task task = (Task) o;
            return task.getTitle().equals(getTitle()) &&
                   task.getDescription().equals(getDescription()) &&
                   task.getState().equals(getState()) &&
                   task.getPriority().equals(getPriority()) &&
                   task.getPlannedStartDate().equals(getPlannedStartDate()) &&
                   task.getPlannedEndDate().equals(getPlannedEndDate()) &&
                   task.getUserClients().equals(getUserClients());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, state, priority, plannedStartDate, plannedEndDate);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    public Set<UserClient> getUserClients() {
        return Collections.unmodifiableSet(userClients);
    }

    public void setUserClients(Set<UserClient> userClients) {
        this.userClients = userClients;
    }

    public void addUserClient(UserClient userClient) {
        userClient.addTask(this);
        this.userClients.add(userClient);
    }

    public void clearUserClients() {
        this.userClients.clear();
    }

    public enum State {
        PLANNED (0),
        IN_PROGRESS (1),
        FINISHED (2);

        private int state;

        State(int state) {
            this.state = state;
        }

        public int toInteger() {
            return this.state;
        }

        public static State getState(int state) {
            switch(state) {
                case 0:
                    return PLANNED;
                case 1:
                    return IN_PROGRESS;
                case 2:
                    return FINISHED;
                default:
                    return FINISHED;
            }
        }
    }

    public enum Priority {
        LOW (0),
        MEDIUM (1),
        HIGH (2),
        URGENT (3);

        private int priority;

        Priority(int priority) {
            this.priority = priority;
        }

        public int toInteger() {
            return this.priority;
        }

        public static Priority getPriority(int priority) {
            switch(priority) {
                case 0:
                    return LOW;
                case 1:
                    return MEDIUM;
                case 2:
                    return HIGH;
                case 3:
                    return URGENT;
                default:
                    return URGENT;
            }
        }
    }

}
