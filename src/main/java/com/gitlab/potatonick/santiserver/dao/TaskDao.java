/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.dao;

import com.gitlab.potatonick.santiserver.controller.HibernateFactory;
import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;
import java.util.Set;

public class TaskDao {

    public boolean insertTask(Task task) {
        return Helper.executeDMLTask((Session session) -> {
            final Set<UserClient> assignedClients = new HashSet<>();
            assignedClients.addAll(task.getUserClients());
            task.clearUserClients();

            session.save(task);
            for (final UserClient userClient : assignedClients) {
                final SQLQuery insertUserTaskQuery = session.createSQLQuery("insert into user_client_task (user_client_id, task_id) values (?, ?)");
                insertUserTaskQuery.setLong(0, userClient.getId());
                insertUserTaskQuery.setLong(1, task.getId());
                insertUserTaskQuery.executeUpdate();
            }
        });
    }

    public boolean updateTask(Task task) {
        return Helper.executeDMLTask((Session session) -> {
            final Set<UserClient> assignedClients = new HashSet<>();
            assignedClients.addAll(task.getUserClients());
            task.clearUserClients();

            // Remove previous assigned users
            final SQLQuery deleteAssignedUsers = session.createSQLQuery("delete from user_client_task where task_id=?");
            deleteAssignedUsers.setLong(0, task.getId());
            deleteAssignedUsers.executeUpdate();

            // Assign new users
            session.update(task);
            for (final UserClient userClient : assignedClients) {
                final SQLQuery assignUserClients = session.createSQLQuery("insert into user_client_task (user_client_id, task_id) values (?, ?)");
                assignUserClients.setLong(0, userClient.getId());
                assignUserClients.setLong(1, task.getId());
                assignUserClients.executeUpdate();
            }
        });
    }

    public boolean deleteTask(long id) {
        return Helper.executeDMLTask((Session session) -> {
            final Task task = findTaskByID(id);
            if (task == null) {
                throw new HibernateError("Task does not exist"); // FIXME: This is a nasty workaround for telling Helper class to return false
            }

            // Delete from user_client_task
            final SQLQuery deleteUserClientTask = session.createSQLQuery("delete from user_client_task where task_id=?");
            deleteUserClientTask.setLong(0, task.getId());
            deleteUserClientTask.executeUpdate();

            // Delete from task
            final SQLQuery deleteTask = session.createSQLQuery("delete from task where id=?");
            deleteTask.setLong(0, task.getId());
            deleteTask.executeUpdate();
        });
    }

    public Task findTaskByID(long id) {
        return Helper.executeDQLTask((Session session) -> session.get(Task.class, id));
    }

    public Set<Task> getAllTasksForUserClient(UserClient userClient) {
        final Session session = HibernateFactory.getInstance().getSessionFactory().openSession();
        final UserClient userClientInDB = session.get(UserClient.class, userClient.getId());
        Set tasks = null;
        if (userClientInDB != null) {
            tasks = userClientInDB.getTasks();
        }
        session.close();
        return tasks;
    }

    Task findTaskByTitle(String title) {
        return Helper.executeDQLTask((Session session) -> {
            Criteria criteria = session.createCriteria(Task.class);
            criteria.add(Restrictions.eq("title", title));
            return criteria.uniqueResult();
        });
    }

}
