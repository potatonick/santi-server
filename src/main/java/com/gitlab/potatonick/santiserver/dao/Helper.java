/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.dao;

import com.gitlab.potatonick.santiserver.controller.HibernateFactory;
import com.gitlab.potatonick.santiserver.entity.Task;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

class Helper {

    private Helper() { }

    static boolean executeDMLTask(DMLCommand command) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateFactory.getInstance().getSessionFactory().openSession();
            transaction = session.beginTransaction();
            command.execute(session);
            transaction.commit();
            return true;
        } catch (HibernateException | IllegalArgumentException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return false;
    }

    static <T> T executeDQLTask(DQLCommand command) {
        Session session = null;
        try {
            session = HibernateFactory.getInstance().getSessionFactory().openSession();
            return (T) command.query(session);
        } catch (HibernateException | IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }

}
