/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.dao;

import com.gitlab.potatonick.santiserver.entity.UserClient;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import java.util.List;

public class UserClientDao {

    public UserClient findUserClientByID(long id) {
        return Helper.executeDQLTask((Session session) -> session.get(UserClient.class, id));
    }

    public UserClient findUserClientByUsername(String username) {
        return Helper.executeDQLTask((Session session) -> {
            Criteria criteria = session.createCriteria(UserClient.class);
            criteria.add(Restrictions.eq("userName", username));
            return criteria.uniqueResult();
        });
    }

    public boolean insertUserClient(UserClient userClient) {
        return Helper.executeDMLTask((Session session) -> session.saveOrUpdate(userClient));
    }

    public List<UserClient> getAllUserClients() {
        return Helper.executeDQLTask((Session session) -> {
            final Criteria criteria = session.createCriteria(UserClient.class);
            return criteria.list();
        });
    }

}
