/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

class RequestEncoder {

    RequestEncoder() { }

    String generateResponse(Status status, ErrorCode errorCode) {
        return generateJSONObjectFromRequest(status, errorCode).toJSONString();
    }

    String generateResponse(long userClientID) {
        final JSONObject response = generateJSONObjectFromRequest(Status.OK, ErrorCode.NONE);
        response.put("data", userClientID);
        return response.toJSONString();
    }

    String generateResponse(Set<Task> tasks) {
        final JSONObject object = generateJSONObjectFromRequest(Status.OK, ErrorCode.NONE);
        final JSONArray data = new JSONArray();
        object.put("data", data);

        for (final Task task : tasks) {
            final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            final JSONObject taskObject = new JSONObject();
            data.add(taskObject);
            taskObject.put("id", task.getId());
            taskObject.put("title", task.getTitle());
            taskObject.put("description", task.getDescription());
            taskObject.put("state", task.getState().toInteger());
            taskObject.put("priority", task.getPriority().toInteger());
            taskObject.put("start_date", dateFormat.format(task.getPlannedStartDate()));
            taskObject.put("end_date", dateFormat.format(task.getPlannedEndDate()));
            final JSONArray users = new JSONArray();
            taskObject.put("users", users);

            for (final UserClient userClient : task.getUserClients()) {
                final JSONObject clientEncoded = encodeUserClient(userClient);
                users.add(clientEncoded);
            }

        }

        return object.toJSONString();
    }

    String generateResponse(List<UserClient> userClients) {
        final JSONObject response = generateJSONObjectFromRequest(Status.OK, ErrorCode.NONE);
        final JSONArray data = new JSONArray();
        response.put("data", data);

        for (final UserClient userClient : userClients) {
            final JSONObject encodedUserClient = new JSONObject();
            data.add(encodedUserClient);

            encodedUserClient.put("id", userClient.getId());
            encodedUserClient.put("real_name", userClient.getRealName());
        }

        return response.toJSONString();
    }

    JSONObject encodeUserClient(UserClient userClient) {
        final JSONObject userClientObject = new JSONObject();
        userClientObject.put("id", userClient.getId());
        userClientObject.put("user_name", userClient.getUserName());
        userClientObject.put("real_name", userClient.getRealName());
        userClientObject.put("role", userClient.getRole().toInteger());
        return userClientObject;
    }

    private JSONObject generateJSONObjectFromRequest(Status status, ErrorCode errorCode) {
        final JSONObject errorObject = new JSONObject();
        errorObject.put("status", status.toString());
        if (status == Status.ERROR) {
            errorObject.put("error", errorCode.toString());
        }
        return errorObject;
    }

}
