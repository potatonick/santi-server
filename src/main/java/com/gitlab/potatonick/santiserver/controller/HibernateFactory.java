/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateFactory {

    private static HibernateFactory INSTANCE;
    private static HibernateConfiguration hibernateConfiguration = new HibernateConfiguration();

    private final Configuration configuration;
    private final SessionFactory sessionFactory;

    private HibernateFactory(Mode mode) {
        this.configuration = new Configuration().configure();
        this.configuration.setProperty("hibernate.hbm2ddl.auto", mode.toString());
        this.configuration.setProperty("hibernate.connection.url", hibernateConfiguration.getHost());
        this.configuration.setProperty("hibernate.connection.username", hibernateConfiguration.getUser());
        this.configuration.setProperty("hibernate.connection.password", hibernateConfiguration.getPassword());
        this.sessionFactory = configuration.buildSessionFactory();
    }

    public static synchronized void setConfiguration(HibernateConfiguration configuration) {
        hibernateConfiguration = configuration;
    }

    public static synchronized HibernateFactory getInstance() {
        return getInstance(Mode.VALIDATE);
    }

    public static synchronized HibernateFactory getInstance(Mode mode) {
        if (INSTANCE == null) {
            INSTANCE = new HibernateFactory(mode);
        }

        return INSTANCE;
    }

    public static synchronized void close() {
        if (INSTANCE != null) {
            getInstance().getSessionFactory().close();
            INSTANCE = null;
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public enum Mode {
        CREATE ("create"),
        CREATE_DROP ("create-drop"),
        VALIDATE ("validate");

        private final String mode;

        Mode(String mode) {
            this.mode = mode;
        }

        @Override
        public String toString() {
            return this.mode;
        }
    }

}
