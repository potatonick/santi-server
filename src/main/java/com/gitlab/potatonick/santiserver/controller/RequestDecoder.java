/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

import com.gitlab.potatonick.santiserver.dao.UserClientDao;
import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import com.gitlab.potatonick.santiserver.helpers.JSONHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

class RequestDecoder {

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final JSONParser parser = new JSONParser();
    private final UserClientDao userClientDao = new UserClientDao();
    private final String request;

    RequestDecoder(String request) {
        this.request = request;
    }

    String getCommand() {
        return getValue("command");
    }

    String getUser() {
        return getValue("user");
    }

    String getPassword() {
        return getValue("password");
    }

    Task getTask() {
        Task task = null;
        try {
            final JSONObject object = (JSONObject) parser.parse(request);
            final JSONObject encodedTask = (JSONObject) object.get("data");
            if (encodedTask == null) {
                return null;
            }

            task = new Task();
            task.setId(JSONHelper.getValue(encodedTask, "id", Long.class));

            task.setTitle(JSONHelper.getValue(encodedTask, "title", String.class));
            task.setDescription(JSONHelper.getValue(encodedTask, "description", String.class));

            final int state = JSONHelper.getValue(encodedTask, "state", Integer.class);
            task.setState(Task.State.getState(state));

            final int priority = JSONHelper.getValue(encodedTask, "priority", Integer.class);
            task.setPriority(Task.Priority.getPriority(priority));

            final String startDate = JSONHelper.getValue(encodedTask, "start-date", String.class);
            task.setPlannedStartDate(decodeDate(startDate));

            final String endDate = JSONHelper.getValue(encodedTask, "end-date", String.class);
            task.setPlannedEndDate(decodeDate(endDate));

            final JSONArray encodedUserClients = JSONHelper.getValue(encodedTask, "users", JSONArray.class);
            for (int i = 0; i < encodedUserClients.size(); i++) {
                final Long userID = JSONHelper.getValue(encodedUserClients, i, Long.class);
                final UserClient findUserClient = userClientDao.findUserClientByID(userID);
                task.addUserClient(findUserClient);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return task;
    }

    Long getID() {
        try {
            final JSONObject object = (JSONObject) parser.parse(request);
            return (Long) object.get("data");
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getValue(String key) {
        try {
            final JSONObject object = (JSONObject) parser.parse(request);
            return JSONHelper.getValue(object, key, String.class);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    private Date decodeDate(String date) {
        try {
            return dateFormat.parse(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

}
