/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

import com.gitlab.potatonick.santiserver.dao.TaskDao;
import com.gitlab.potatonick.santiserver.dao.UserClientDao;
import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Controller {

    private final RequestEncoder requestEncoder = new RequestEncoder();
    private final TaskDao taskDao = new TaskDao();
    private final UserClientDao userClientDao = new UserClientDao();

    public Controller() { }

    public String executeRequestAndGetResponse(String request) {
        final RequestDecoder requestDecoder = new RequestDecoder(request);
        final String user = requestDecoder.getUser();
        final String password = requestDecoder.getPassword();

        switch (requestDecoder.getCommand()) {
            case "check-credentials":
                if (isAuthorized(user, password)) {
                    final long clientID = getClientIDByUsername(user);
                    return requestEncoder.generateResponse(clientID);
                } else {
                    return requestEncoder.generateResponse(Status.ERROR, ErrorCode.UNAUTHORIZED);
                }
            case "get-tasks":
                return getTasksAndGetResponse(user, password);
            case "create-task":
                return createTaskAndGetResponse(user, password, requestDecoder);
            case "remove-task":
                return removeTaskAndGetResponse(user, password, requestDecoder);
            case "update-task":
                return updateTaskAndGetResponse(user, password, requestDecoder);
            case "get-users":
                return getUsers(user, password);
            default:
                return requestEncoder.generateResponse(Status.ERROR, ErrorCode.UNKNOWN_COMMAND);
        }
    }

    private boolean isAuthorized(String user, String password) {
        final UserClient userClient = userClientDao.findUserClientByUsername(user);
        if (userClient == null) {
            return false;
        }

        return userClient.getPassword().equals(password);
    }

    private long getClientIDByUsername(String user) {
        final UserClient userClient = userClientDao.findUserClientByUsername(user);
        if (userClient == null) {
            return -1;
        } else {
            return userClient.getId();
        }
    }

    private String getTasksAndGetResponse(String user, String password) {
        if (! isAuthorized(user, password)) {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.UNAUTHORIZED);
        }

        final UserClient userClient = userClientDao.findUserClientByUsername(user);
        final Set<Task> tasks = taskDao.getAllTasksForUserClient(userClient);
        return requestEncoder.generateResponse(tasks);
    }

    private String createTaskAndGetResponse(String user, String password, RequestDecoder requestDecoder) {
        if (! isAuthorized(user, password)) {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.UNAUTHORIZED);
        }

        final Task task = requestDecoder.getTask();
        if (task == null) {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.SERVER_ERROR);
        }

        final boolean insertSuccess = taskDao.insertTask(task);
        if (insertSuccess) {
            return requestEncoder.generateResponse(Status.OK, ErrorCode.NONE);
        } else {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.SERVER_ERROR);
        }
    }

    private String removeTaskAndGetResponse(String user, String password, RequestDecoder requestDecoder) {
        if (! isAuthorized(user, password)) {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.UNAUTHORIZED);
        }

        final long taskID = requestDecoder.getID();
        final boolean removeSuccess = taskDao.deleteTask(taskID);
        if (removeSuccess) {
            return requestEncoder.generateResponse(Status.OK, ErrorCode.NONE);
        } else {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.SERVER_ERROR);
        }
    }

    private String updateTaskAndGetResponse(String user, String password, RequestDecoder requestDecoder) {
        if (! isAuthorized(user, password)) {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.UNAUTHORIZED);
        }

        final Task updateTask = requestDecoder.getTask();
        final boolean updateSuccess = taskDao.updateTask(updateTask);
        if (updateSuccess) {
            return requestEncoder.generateResponse(Status.OK, ErrorCode.NONE);
        } else {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.SERVER_ERROR);
        }
    }

    private String getUsers(String user, String password) {
        if (! isAuthorized(user, password)) {
            return requestEncoder.generateResponse(Status.ERROR, ErrorCode.UNAUTHORIZED);
        }

        List<UserClient> userClients = userClientDao.getAllUserClients();
        if (userClients == null) {
            userClients = new ArrayList<>();
        }
        return requestEncoder.generateResponse(userClients);
    }

}
