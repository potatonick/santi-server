/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

public class HibernateConfiguration {

    private String host = "";
    private String user = "";
    private String password = "";

    public HibernateConfiguration() { }

    public String getHost() {
        return this.host;
    }

    public String getUser() {
        return this.user;
    }

    public String getPassword() {
        return this.password;
    }

    public HibernateConfiguration setHost(String host) {
        this.host = "jdbc:mysql://" + host;
        return this;
    }

    public HibernateConfiguration setUser(String user) {
        this.user = user;
        return this;
    }

    public HibernateConfiguration setPassword(String password) {
        this.password = password;
        return this;
    }

}
