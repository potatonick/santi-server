/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver;

import com.gitlab.potatonick.santiserver.controller.HibernateConfiguration;
import com.gitlab.potatonick.santiserver.controller.HibernateFactory;
import com.gitlab.potatonick.santiserver.dao.UserClientDao;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import com.gitlab.potatonick.santiserver.socket.NetworkListener;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Main {

    private static final String USER_ENVVAR = "SANTI_USER";
    private static final String PASSWORD_ENVVAR = "SANTI_PASSWORD";
    private static final String HOST_ENVVAR = "SANTI_HOST";

    private static final Map<String, String> ENVIRONMENT_VARIABLES = new HashMap<>();
    private static final Logger LOGGER = Logger.getLogger(Main.class.getPackage().toString());

    private static boolean createTables = false;
    private static boolean createUser = false;

    private static String nick = "";
    private static String realName = "";
    private static String password = "";

    static {
        ENVIRONMENT_VARIABLES.put(USER_ENVVAR, null);
        ENVIRONMENT_VARIABLES.put(PASSWORD_ENVVAR, null);
        ENVIRONMENT_VARIABLES.put(HOST_ENVVAR, null);
    }

    public static void main(String[] args) {
        readEnvironmentVariables();
        parseCommandLineArguments(args);
        if (allEnvvarsAreInitialized()) {
            configureHibernate();
        } else {
            return;
        }

        if (createTables) {
            createTables();
        } else if (createUser) {
            createUser();
        } else {
            startServer();
        }
    }

    private static void parseCommandLineArguments(String[] args) {
        if (args.length <= 0) {
            return;
        }

        final String firstArgument = args[0];
        switch (firstArgument) {
            case "--create-tables":
                createTables = true;
            case "--create-user":
                if (args.length >= 4) {
                    nick = args[1];
                    realName = args[2];
                    password = args[3];
                }

                createUser = true;
        }
    }

    private static void createTables() {
        HibernateFactory.getInstance(HibernateFactory.Mode.CREATE);
        HibernateFactory.close();
    }

    private static void createUser() {
        if (nick.isEmpty() || realName.isEmpty() || password.isEmpty()) {
            LOGGER.log(Level.SEVERE, "Arguments missing. Usage -> --create-user \"nick\" \"name\" \"password\"");
            return;
        }

        HibernateFactory.getInstance();
        final UserClient userClient = new UserClient();
        userClient.setUserName(nick);
        userClient.setRealName(realName);
        userClient.setPassword(password);

        final UserClientDao userClientDao = new UserClientDao();
        userClientDao.insertUserClient(userClient);
        HibernateFactory.close();
    }

    private static void readEnvironmentVariables() {
        for (Map.Entry<String, String> envvar : ENVIRONMENT_VARIABLES.entrySet()) {
            final String envvar_name = envvar.getKey();
            final String envvar_value = System.getenv(envvar_name);
            envvar.setValue(envvar_value);
        }
    }

    private static void configureHibernate() {
        HibernateFactory.setConfiguration(
                new HibernateConfiguration()
                        .setHost(ENVIRONMENT_VARIABLES.get(HOST_ENVVAR))
                        .setUser(ENVIRONMENT_VARIABLES.get(USER_ENVVAR))
                        .setPassword(ENVIRONMENT_VARIABLES.get(PASSWORD_ENVVAR))
        );
    }

    private static void startServer() {
        NetworkListener server = new NetworkListener();
        server.listen();
        server.close();
    }

    private static boolean allEnvvarsAreInitialized() {
        for (Map.Entry<String, String> envvar : ENVIRONMENT_VARIABLES.entrySet()) {
            final String envvar_value = envvar.getValue();
            if (isEnvvarNotInitialized(envvar_value)) {
                final String envvar_name = envvar.getKey();
                logEnvvarNotInitialized(envvar_name);
                return false;
            }
        }
        return true;
    }

    private static boolean isEnvvarNotInitialized(String envvar) {
        return envvar == null || envvar.isEmpty();
    }

    private static void logEnvvarNotInitialized(String envvar) {
        LOGGER.log(Level.SEVERE, String.format("The required %s environment variable is not initialized!", envvar));
    }

}
