/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.socket;

import com.gitlab.potatonick.santiserver.controller.HibernateFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NetworkListener {

    public static final int PORT = 48576;

    private ServerSocket serverSocket;
    private boolean active;
    private boolean close;

    private final Logger logger;

    public NetworkListener() {
        this.active = false;
        this.close = false;
        HibernateFactory.getInstance();
        this.logger = Logger.getLogger(this.getClass().getPackage().toString());
    }

    public synchronized void listen() {
        if (! active) {
            try {
                serverSocket = new ServerSocket(PORT);
                active = true;
                logger.log(Level.INFO, "Server listening on port " + serverSocket.getLocalPort());

                try {
                    while (!close) {
                        Socket socket = serverSocket.accept();
                        ClientThread client = new ClientThread(socket);
                        client.run();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close();
            }

            logger.log(Level.INFO, "Server closed");
        }
    }

    public synchronized void close() {
        if (active) {
            closeServerSocket();
            close = true;
            active = false;
        }

        HibernateFactory.close();
    }

    private void closeServerSocket() {
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
