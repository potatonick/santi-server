/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.socket;

import com.gitlab.potatonick.santiserver.controller.Controller;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

class ClientThread extends Thread {

    private final Socket clientSocket;
    private final PrintWriter outputStream;
    private final BufferedReader inputStream;
    private final Controller controller;
    private boolean closed;

    private final Logger logger;

    ClientThread(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        this.outputStream = new PrintWriter(
                new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8"),
                true
        );
        this.inputStream = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream(), "UTF-8")
        );
        this.controller = new Controller();
        this.closed = false;
        this.logger = Logger.getLogger(this.getClass().getPackage().toString());

        final String clientIP = clientSocket.getInetAddress().getHostAddress();
        setName(String.format("client-%s-thread", clientIP));
    }

    @Override
    public void run() {
        final String clientIP = clientSocket.getInetAddress().getHostAddress();
        logger.log(Level.INFO, "Client connected with IP " + clientIP);

        final String request = getRequest();
        final String response = controller.executeRequestAndGetResponse(request);
        sendResponse(response);
        closeClientSocket();

        logger.log(Level.INFO, "Client closed with IP " + clientIP);
    }

    private String getRequest() {
        try {
            return inputStream.readLine();
        } catch (IOException e) {
            closed = true;
            e.printStackTrace();
        }
        return "";
    }

    private void sendResponse(String response) {
        if (closed) {
            return;
        }

        if (outputStream.checkError()) {
            closed = true;
            return;
        }

        outputStream.printf(response);
    }

    private void closeClientSocket() {
        try {
            if (! closed) {
                clientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
