/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.dao;

import com.gitlab.potatonick.santiserver.controller.HibernateFactory;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import com.gitlab.potatonick.santiserver.helpers.DaoHelper;
import org.junit.*;

import java.util.UUID;

public class UserClientDaoTest {

    private final UserClientDao userClientDao;
    private UserClient userClient;

    public UserClientDaoTest() {
        this.userClientDao = new UserClientDao();
    }

    @BeforeClass
    public static void createCleanSession() {
        // WARNING: tables will be erased when this test runs
        HibernateFactory.close();
        HibernateFactory.getInstance(HibernateFactory.Mode.CREATE_DROP);
    }

    @AfterClass
    public static void closeSessionFactory() {
        HibernateFactory.close();
    }

    @Before
    public void createRandomClient() {
        this.userClient = DaoHelper.createRandomClient();
    }

    @Test
    public void shouldFindExistingUserClientByID() {
        // Insert the client in the DB
        final String userClientName = userClient.getUserName();
        Assert.assertTrue(userClientDao.insertUserClient(userClient));

        // Get the generated ID
        final UserClient findUserClient = userClientDao.findUserClientByUsername(userClientName);
        final long findClientID = findUserClient.getId();
        Assert.assertNotNull(findUserClient);

        // Retrieve that user by ID
        final UserClient userClientByID = userClientDao.findUserClientByID(findClientID);
        Assert.assertNotNull(userClientByID);
        Assert.assertEquals(findUserClient, userClientByID);
    }

    @Test
    public void shouldFindExistingUserClientByUsername() {
        // Insert the client in the DB
        final String userClientName = userClient.getUserName();
        Assert.assertTrue(userClientDao.insertUserClient(userClient));

        // Retrieve that user
        final UserClient findUserClient = userClientDao.findUserClientByUsername(userClientName);
        Assert.assertNotNull(findUserClient);
        Assert.assertEquals(userClientName, findUserClient.getUserName());
    }

    @Test
    public void shouldNotFindNonExistingUsername() {
        final String userClientName = "Pepito Perez";
        final UserClient findUserClient = userClientDao.findUserClientByUsername(userClientName);
        Assert.assertNull(findUserClient);
    }

}
