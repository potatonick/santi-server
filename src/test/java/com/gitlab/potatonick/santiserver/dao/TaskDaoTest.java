/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.dao;

import com.gitlab.potatonick.santiserver.controller.HibernateFactory;
import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import com.gitlab.potatonick.santiserver.helpers.DaoHelper;
import org.junit.*;

import java.text.ParseException;
import java.util.Set;

public class TaskDaoTest {

    private final TaskDao taskDao;
    private final UserClientDao userClientDao;
    private Task task;
    private UserClient userClient;

    public TaskDaoTest() {
        this.userClientDao = new UserClientDao();
        this.taskDao = new TaskDao();
    }

    @BeforeClass
    public static void createCleanSession() {
        // WARNING: tables will be erased when this test runs
        HibernateFactory.close();
        HibernateFactory.getInstance(HibernateFactory.Mode.CREATE_DROP);
    }

    @AfterClass
    public static void closeSessionFactory() {
        HibernateFactory.close();
    }

    @Before
    public void createSingleRandomTask() throws ParseException {
        this.task = DaoHelper.createRandomTask();
        this.userClient = DaoHelper.createRandomClient();
    }

    @Test
    public void shouldInsertNewTask() {
        final String taskTitle = task.getTitle();

        Assert.assertTrue(taskDao.insertTask(task));
        final Task task = taskDao.findTaskByTitle(taskTitle);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskTitle, task.getTitle());
    }

    @Test
    public void shouldInsertDuplicateTasks() throws ParseException {
        // Insert client
        Assert.assertTrue(userClientDao.insertUserClient(userClient));

        // Insert two duplicated and independent tasks
        final Task originalTask = DaoHelper.createRandomTask();
        task.addUserClient(userClient);
        final Task duplicatedTask = duplicateTask(originalTask);
        duplicatedTask.addUserClient(userClient);

        Assert.assertTrue(taskDao.insertTask(originalTask));
        Assert.assertTrue(taskDao.insertTask(duplicatedTask));

        // Check client has both tasks
        final Task findOriginalTask = taskDao.findTaskByID(originalTask.getId());
        final Task findDuplicatedTask = taskDao.findTaskByID(duplicatedTask.getId());
        Assert.assertNotNull(findOriginalTask);
        Assert.assertNotNull(findDuplicatedTask);
        Assert.assertEquals(findOriginalTask, originalTask);
        Assert.assertEquals(findDuplicatedTask, duplicatedTask);
    }

    @Test
    public void shouldUpdateExistingTask() {
        // Insert task
        final String originalTaskTitle = task.getTitle();
        Assert.assertTrue(taskDao.insertTask(task));

        // Update task
        task.setTitle(DaoHelper.createRandomTitle());
        final String newTaskTitle = task.getTitle();
        Assert.assertTrue(taskDao.updateTask(task));

        // Check changes
        final Task task = taskDao.findTaskByTitle(newTaskTitle);
        Assert.assertNotNull(task);
        Assert.assertNotEquals(originalTaskTitle, task.getTitle());
        Assert.assertEquals(newTaskTitle, task.getTitle());
    }

    @Test
    public void shouldNotUpdateNonExistingTask() {
        // Try to update non-existing task
        final String title = task.getTitle();
        Assert.assertFalse(taskDao.updateTask(task));

        // Check changes
        final Task task = taskDao.findTaskByTitle(title);
        Assert.assertNull(task);
    }

    @Test
    public void shouldDeleteExistingTask() {
        // Insert task
        final String taskTitle = task.getTitle();
        Assert.assertTrue(taskDao.insertTask(task));

        // Check it is saved
        Task task = taskDao.findTaskByTitle(taskTitle);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskTitle, task.getTitle());

        // Remove task
        Assert.assertTrue(taskDao.deleteTask(task.getId()));

        // Check changes
        task = taskDao.findTaskByTitle(taskTitle);
        Assert.assertNull(task);
    }

    @Test
    public void shouldNotDeleteNonExistingTask() {
        // Try remove task
        final long taskID = task.getId();
        final String taskTitle = task.getTitle();
        Assert.assertFalse(taskDao.deleteTask(taskID));

        // Check changes
        final Task task = taskDao.findTaskByTitle(taskTitle);
        Assert.assertNull(task);
    }

    @Test
    public void shouldNotRemoveUserClientWhenDeletingTask() {
        // Insert user
        Assert.assertTrue(userClientDao.insertUserClient(userClient));
        final UserClient findUserClient = userClientDao.findUserClientByUsername(userClient.getUserName());
        Assert.assertNotNull(findUserClient);
        this.task.addUserClient(userClient);

        // Insert task
        taskDao.insertTask(task);

        // Remove task
        Assert.assertTrue(taskDao.deleteTask(task.getId()));
        final UserClient findAgainUserClient = userClientDao.findUserClientByUsername(userClient.getUserName());
        Assert.assertNotNull(findAgainUserClient);
    }

    @Test
    public void shouldGetAllTasksForUserClient() throws ParseException {
        // Insert client
        Assert.assertTrue(userClientDao.insertUserClient(userClient));

        // Insert tasks
        final Task[] mockTasks = createRandomTasks();
        Assert.assertTrue(taskDao.insertTask(mockTasks[0]));

        Set<Task> savedTasks = taskDao.getAllTasksForUserClient(userClient);
        Assert.assertNotNull(savedTasks);
        Assert.assertEquals(10, savedTasks.size());
    }

    private Task[] createRandomTasks() throws ParseException {
        Task[] tasks = new Task[10];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = DaoHelper.createRandomTask();
            tasks[i].addUserClient(userClient);
        }
        return tasks;
    }

    private Task duplicateTask(Task task) {
        final Task duplicatedTask = new Task();
        duplicatedTask.setTitle(task.getTitle());
        duplicatedTask.setDescription(task.getDescription());
        duplicatedTask.setState(task.getState());
        duplicatedTask.setPriority(task.getPriority());
        duplicatedTask.setPlannedStartDate(task.getPlannedStartDate());
        duplicatedTask.setPlannedEndDate(task.getPlannedEndDate());
        return duplicatedTask;
    }

}
