/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.helpers;

import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;

public class DaoHelper {

    private DaoHelper() { }

    public static Task createRandomTask() throws ParseException {
        final Task task = new Task();
        task.setTitle(createRandomTitle()); // This is used as unique identification for the tests
        task.setDescription("Description");
        task.setState(Task.State.PLANNED);
        task.setPriority(Task.Priority.LOW);
        task.setPlannedStartDate(
                new SimpleDateFormat("dd-MM-yyyy").parse("15-06-2007")
        );
        task.setPlannedEndDate(
                new SimpleDateFormat("dd-MM-yyyy").parse("17-07-2007")
        );
        return task;
    }

    public static String createRandomTitle() {
        String title = UUID.randomUUID().toString();
        title = title.substring(0, Math.min(10, title.length()));
        return title;
    }

    public static UserClient createRandomClient() {
        final UserClient userClient = new UserClient();
        userClient.setUserName(createRandomTitle());
        userClient.setRealName("Real name");
        userClient.setPassword("1234");
        userClient.setRole(UserClient.Role.USER);
        return userClient;
    }

}
