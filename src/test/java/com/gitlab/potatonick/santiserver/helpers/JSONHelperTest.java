/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.helpers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class JSONHelperTest {

    private static final String STRING_KEY = "string";
    private static final int STRING_INDEX = 0;
    private static final String STRING_VALUE = "ok";

    private static final String INTEGER_KEY = "integer";
    private static final int INTEGER_INDEX = 1;
    private static final Integer INTEGER_VALUE = 82;

    private static final String LONG_KEY = "long";
    private static final int LONG_INDEX = 2;
    private static final Long LONG_VALUE = 56L;

    private static final String FLOAT_KEY = "float";
    private static final int FLOAT_INDEX = 3;
    private static final Float FLOAT_VALUE = 0.0f;

    private static final String DOUBLE_KEY = "double";
    private static final int DOUBLE_INDEX = 4;
    private static final Double DOUBLE_VALUE = 82.34;

    private static final String BOOLEAN_TRUE_KEY = "boolean-true";
    private static final int BOOLEAN_TRUE_INDEX = 5;

    private static final String BOOLEAN_FALSE_KEY = "boolean-false";
    private static final int BOOLEAN_FALSE_INDEX = 6;

    private static final String NULL_EXPLICIT_KEY = "null";
    private static final int NULL_EXPLICIT_INDEX = 7;

    private static final String ARRAY_KEY = "array";
    private static final int ARRAY_INDEX = 8;

    private static final String OBJECT_KEY = "object";
    private static final int OBJECT_INDEX = 9;

    private static final String NON_EXISTING_KEY = "i-made-up-this-key-on-purpose";
    private static final int NON_EXISTING_INDEX = 100;



    private static final JSONParser JSON_PARSER = new JSONParser();
    private static JSONObject JSON_OBJECT;
    private static JSONArray JSON_ARRAY;

    private static final String JSON_WITH_OBJECT = "" +
            "{" +
                "\"" + STRING_KEY + "\": \"" + STRING_VALUE + "\"," +
                "\"" + INTEGER_KEY + "\": " + INTEGER_VALUE + "," +
                "\"" + LONG_KEY + "\": " + LONG_VALUE + "," +
                "\"" + FLOAT_KEY + "\": " + FLOAT_VALUE + "," +
                "\"" + DOUBLE_KEY + "\": " + DOUBLE_VALUE + "," +
                "\"" + BOOLEAN_TRUE_KEY + "\": true," +
                "\"" + BOOLEAN_FALSE_KEY + "\": false," +
                "\"" + NULL_EXPLICIT_KEY + "\": null," +
                "\"" + ARRAY_KEY + "\": []," +
                "\"" + OBJECT_KEY + "\": {}" +
            "}";
    private static final String JSON_WITH_ARRAY = "" +
            "[" +
                "\"" + STRING_VALUE + "\"," +
                INTEGER_VALUE + ", " +
                LONG_VALUE + "," +
                FLOAT_VALUE + ", " +
                DOUBLE_VALUE + "," +
                "true," +
                "false," +
                "null," +
                "[]," +
                "{}" +
            "]";

    @BeforeClass
    public static void parseTestingJSONData() throws ParseException {
        JSON_OBJECT = (JSONObject) JSON_PARSER.parse(JSON_WITH_OBJECT);
        JSON_ARRAY = (JSONArray) JSON_PARSER.parse(JSON_WITH_ARRAY);
    }

    @Test
    public void shouldReturnStringValueFromObject() {
        final String value = JSONHelper.getValue(JSON_OBJECT, STRING_KEY, String.class);
        Assert.assertEquals(STRING_VALUE, value);
    }

    @Test
    public void shouldReturnStringValueFromArray() {
        final String value = JSONHelper.getValue(JSON_ARRAY, STRING_INDEX, String.class);
        Assert.assertEquals(value, STRING_VALUE);
    }

    @Test
    public void shouldReturnIntegerValueFromObject() {
        final Integer value = JSONHelper.getValue(JSON_OBJECT, INTEGER_KEY, Integer.class);
        Assert.assertEquals(INTEGER_VALUE, value);
    }

    @Test
    public void shouldReturnIntegerValueFromArray() {
        final Integer value = JSONHelper.getValue(JSON_ARRAY, INTEGER_INDEX, Integer.class);
        Assert.assertEquals(INTEGER_VALUE, value);
    }

    @Test
    public void shouldReturnLongValueFromObject() {
        final Long value = JSONHelper.getValue(JSON_OBJECT, LONG_KEY, Long.class);
        Assert.assertEquals(LONG_VALUE, value);
    }

    @Test
    public void shouldReturnLongValueFromArray() {
        final Long value = JSONHelper.getValue(JSON_ARRAY, LONG_INDEX, Long.class);
        Assert.assertEquals(LONG_VALUE, value);
    }

    @Test
    public void shouldReturnFloatValueFromObject() {
        final Float value = JSONHelper.getValue(JSON_OBJECT, FLOAT_KEY, Float.class);
        Assert.assertEquals(FLOAT_VALUE, value);
    }

    @Test
    public void shouldReturnFloatValueFromArray() {
        final Float value = JSONHelper.getValue(JSON_ARRAY, FLOAT_INDEX, Float.class);
        Assert.assertEquals(FLOAT_VALUE, value);
    }

    @Test
    public void shouldReturnDoubleValueFromObject() {
        final Double value = JSONHelper.getValue(JSON_OBJECT, DOUBLE_KEY, Double.class);
        Assert.assertEquals(DOUBLE_VALUE, value);
    }

    @Test
    public void shouldReturnDoubleValueFromArray() {
        final Double value = JSONHelper.getValue(JSON_ARRAY, DOUBLE_INDEX, Double.class);
        Assert.assertEquals(DOUBLE_VALUE, value);
    }

    @Test
    public void shouldReturnBooleanTrueValueFromObject() {
        final Boolean value = JSONHelper.getValue(JSON_OBJECT, BOOLEAN_TRUE_KEY, Boolean.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value);
    }

    @Test
    public void shouldReturnBooleanTrueValueFromArray() {
        final Boolean value = JSONHelper.getValue(JSON_ARRAY, BOOLEAN_TRUE_INDEX, Boolean.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value);
    }

    @Test
    public void shouldReturnBooleanFalseValueFromObject() {
        final Boolean value = JSONHelper.getValue(JSON_OBJECT, BOOLEAN_FALSE_KEY, Boolean.class);
        Assert.assertNotNull(value);
        Assert.assertFalse(value);
    }

    @Test
    public void shouldReturnBooleanFalseValueFromArray() {
        final Boolean value = JSONHelper.getValue(JSON_ARRAY, BOOLEAN_FALSE_INDEX, Boolean.class);
        Assert.assertNotNull(value);
        Assert.assertFalse(value);
    }

    @Test
    public void shouldReturnExplicitNullFromObject() {
        // There is a null value explicitly in the JSON data
        final Object value = JSONHelper.getValue(JSON_OBJECT, NULL_EXPLICIT_KEY, Object.class);
        Assert.assertNull(value);
    }

    @Test
    public void shouldReturnExplicitNullFromArray() {
        // There is a null value explicitly in the JSON data
        final Object value = JSONHelper.getValue(JSON_ARRAY, NULL_EXPLICIT_INDEX, Object.class);
        Assert.assertNull(value);
    }

    @Test
    public void shouldReturnArrayValueFromObject() {
        final JSONArray value = JSONHelper.getValue(JSON_OBJECT, ARRAY_KEY, JSONArray.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnArrayValueFromArray() {
        final JSONArray value = JSONHelper.getValue(JSON_ARRAY, ARRAY_INDEX, JSONArray.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnObjectValueFromObject() {
        final JSONObject value = JSONHelper.getValue(JSON_OBJECT, OBJECT_KEY, JSONObject.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnObjectValueFromArray() {
        final JSONObject value = JSONHelper.getValue(JSON_ARRAY, OBJECT_INDEX, JSONObject.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnEmptyStringWhenKeyDoesntExistFromObject() {
        final String value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, String.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnEmptyStringWhenIndexDoesntExistFromArray() {
        final String value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, String.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnIntegerZeroWhenKeyDoesntExistFromObject() {
        final Number value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, Integer.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0, value);
    }

    @Test
    public void shouldReturnIntegerZeroWhenIndexDoesntExistFromArray() {
        final Number value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, Integer.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0, value);
    }

    @Test
    public void shouldReturnLongZeroWhenKeyDoesntExistFromObject() {
        final Number value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, Long.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0L, value);
    }

    @Test
    public void shouldReturnLongZeroWhenIndexDoesntExistFromArray() {
        final Number value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, Long.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0L, value);
    }

    @Test
    public void shouldReturnFloatZeroWhenKeyDoesntExistFromObject() {
        final Number value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, Float.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0.0f, value);
    }

    @Test
    public void shouldReturnFloatZeroWhenIndexDoesntExistFromArray() {
        final Number value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, Float.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0.0f, value);
    }

    @Test
    public void shouldReturnDoubleZeroWhenKeyDoesntExistFromObject() {
        final Number value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, Double.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0.0, value);
    }

    @Test
    public void shouldReturnDoubleZeroWhenIndexDoesntExistFromArray() {
        final Number value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, Double.class);
        Assert.assertNotNull(value);
        Assert.assertEquals(0.0, value);
    }

    @Test
    public void shouldReturnFalseWhenKeyDoesntExistFromObject() {
        final Boolean value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, Boolean.class);
        Assert.assertNotNull(value);
        Assert.assertFalse(value);
    }

    @Test
    public void shouldReturnFalseWhenIndexDoesntExistFromArray() {
        final Boolean value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, Boolean.class);
        Assert.assertNotNull(value);
        Assert.assertFalse(value);
    }

    @Test
    public void shouldReturnEmptyArrayWhenKeyDoesntExistFromObject() {
        final JSONArray value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, JSONArray.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnEmptyArrayWhenIndexDoesntExistFromArray() {
        final JSONArray value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, JSONArray.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnEmptyObjectWhenKeyDoesntExistsFromObject() {
        final JSONObject value = JSONHelper.getValue(JSON_OBJECT, NON_EXISTING_KEY, JSONObject.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    @Test
    public void shouldReturnEmptyObjectWhenIndexDoesntExistFromArray() {
        final JSONObject value = JSONHelper.getValue(JSON_ARRAY, NON_EXISTING_INDEX, JSONObject.class);
        Assert.assertNotNull(value);
        Assert.assertTrue(value.isEmpty());
    }

    // TODO: Add tests to ensure it converts correctly between data types

}
