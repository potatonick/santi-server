/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.entity;

import com.gitlab.potatonick.santiserver.helpers.DaoHelper;
import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TaskTest {

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private final String title = "Task title";
    private final String description = "Task description";
    private final Task.State state = Task.State.FINISHED;
    private final Task.Priority priority = Task.Priority.HIGH;
    private final Date startDate = dateFormat.parse("23-06-1996");
    private final Date endDate = dateFormat.parse("01-08-1996");
    private final Set<UserClient> userClients = new HashSet<>();

    private final Task originalTask = new Task();
    private final Task compareTask = new Task();

    public TaskTest() throws ParseException {
        userClients.add(DaoHelper.createRandomClient());
        userClients.add(DaoHelper.createRandomClient());
        userClients.add(DaoHelper.createRandomClient());

        originalTask.setTitle(title);
        originalTask.setDescription(description);
        originalTask.setState(state);
        originalTask.setPriority(priority);
        originalTask.setPlannedStartDate(startDate);
        originalTask.setPlannedEndDate(endDate);
        originalTask.setUserClients(userClients);

        compareTask.setTitle(title);
        compareTask.setDescription(description);
        compareTask.setState(state);
        compareTask.setPriority(priority);
        compareTask.setPlannedStartDate(startDate);
        compareTask.setPlannedEndDate(endDate);
        compareTask.setUserClients(userClients);
    }

    @Test
    public void shouldCompareEqualTasks() {
        Assert.assertEquals(originalTask, compareTask);
    }

    @Test
    public void shouldCompareNotEqualTasks() {
        originalTask.setUserClients(new HashSet<>());
        Assert.assertNotEquals(originalTask, compareTask);
    }

}
