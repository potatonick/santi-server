/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.entity;

import org.junit.Assert;
import org.junit.Test;

public class UserClientTest {

    private final String userName = "pepito_perez";
    private final String realName = "Pepito Pérez";
    private final String password = "1234";
    private final UserClient.Role role = UserClient.Role.ADMINISTRADOR;

    private final UserClient originalUserClient = new UserClient();
    private final UserClient compareUserClient = new UserClient();

    public UserClientTest() {
        originalUserClient.setUserName(userName);
        originalUserClient.setRealName(realName);
        originalUserClient.setPassword(password);
        originalUserClient.setRole(role);

        compareUserClient.setUserName(userName);
        compareUserClient.setRealName(realName);
        compareUserClient.setPassword(password);
        compareUserClient.setRole(role);
    }

    @Test
    public void shouldCompareEqualUserClients() {
        Assert.assertEquals(originalUserClient, compareUserClient);
    }

    @Test
    public void shouldCompareNotEqualUserClients() {
        compareUserClient.setRole(UserClient.Role.USER);
        Assert.assertNotEquals(originalUserClient, compareUserClient);
    }

}
