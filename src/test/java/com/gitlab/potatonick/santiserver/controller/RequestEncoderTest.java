/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import com.gitlab.potatonick.santiserver.helpers.JSONHelper;
import com.gitlab.potatonick.santiserver.helpers.DaoHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class RequestEncoderTest {

    private final JSONParser parser = new JSONParser();
    private final RequestEncoder requestEncoder = new RequestEncoder();

    @Test
    public void shouldGenerateOkResponseWithoutErrorCode() throws ParseException {
        final String response = requestEncoder.generateResponse(Status.OK, ErrorCode.SERVER_ERROR);
        JSONObject object = (JSONObject) parser.parse(response);
        final String status = JSONHelper.getValue(object, "status", String.class);
        final String errorCode = JSONHelper.getValue(object, "error", String.class);

        Assert.assertEquals(Status.OK.toString(), status);
        Assert.assertTrue(errorCode.isEmpty());
    }

    @Test
    public void shouldGenerateFailedResponseWithErrorCode() throws ParseException {
        final String response = requestEncoder.generateResponse(Status.ERROR, ErrorCode.SERVER_ERROR);
        JSONObject object = (JSONObject) parser.parse(response);
        final String status = JSONHelper.getValue(object, "status", String.class);
        final String errorCode = JSONHelper.getValue(object, "error", String.class);

        Assert.assertEquals(Status.ERROR.toString(), status);
        Assert.assertEquals(ErrorCode.SERVER_ERROR.toString(), errorCode);
    }

    @Test
    public void shouldGenerateResponseOfTasks() throws java.text.ParseException, ParseException {
        final UserClient userClient = DaoHelper.createRandomClient();
        final Set<Task> originalTasks = new HashSet<>();
        originalTasks.add(createRandomTaskWithUserClient(userClient));
        originalTasks.add(createRandomTaskWithUserClient(userClient));
        originalTasks.add(createRandomTaskWithUserClient(userClient));

        final String response = requestEncoder.generateResponse(originalTasks);
        Assert.assertNotNull(response);

        // Analize response
        final JSONObject object = (JSONObject) parser.parse(response);
        final String status = JSONHelper.getValue(object, "status", String.class);
        final String errorCode = JSONHelper.getValue(object, "error", String.class);
        final JSONArray decodedTasks = JSONHelper.getValue(object, "data", JSONArray.class);

        Assert.assertEquals(Status.OK.toString(), status);
        Assert.assertTrue(errorCode.isEmpty());
        Assert.assertNotNull(decodedTasks);
        Assert.assertEquals(originalTasks.size(), decodedTasks.size());

        // Analize array of tasks
        for (int i = 0; i < decodedTasks.size(); i++) {
            final JSONObject decodedTask = JSONHelper.getValue(decodedTasks, i, JSONObject.class);
            final String decodedTaskTitle = JSONHelper.getValue(decodedTask, "title", String.class);
            Assert.assertFalse(decodedTaskTitle.isEmpty());

            // Analize user
            final JSONArray decodedClients = JSONHelper.getValue(decodedTask, "users", JSONArray.class);
            Assert.assertNotNull(decodedClients);
            Assert.assertEquals(1, decodedClients.size()); // There's only one user associated with the tasks

            final JSONObject decodedUserClient = JSONHelper.getValue(decodedClients, 0, JSONObject.class);
            final String decodedUserClientName = JSONHelper.getValue(decodedUserClient, "user_name", String.class);
            Assert.assertFalse(decodedUserClientName.isEmpty());
        }
    }

    @Test
    public void shouldGenerateUserClientJSONObject() throws ParseException {
        final UserClient userClient = DaoHelper.createRandomClient();
        final JSONObject userClientEncoded = requestEncoder.encodeUserClient(userClient);

        final long userEncodedID = JSONHelper.getValue(userClientEncoded, "id", Long.class);
        final String userEncodedUsername = JSONHelper.getValue(userClientEncoded, "user_name", String.class);
        final String userEncodedRealName = JSONHelper.getValue(userClientEncoded, "real_name", String.class);
        final String userEncodedPassword = JSONHelper.getValue(userClientEncoded, "password", String.class);
        final int userEncodedRole = JSONHelper.getValue(userClientEncoded, "role", Integer.class);

        Assert.assertEquals(userClient.getId(), userEncodedID);
        Assert.assertEquals(userClient.getUserName(), userEncodedUsername);
        Assert.assertEquals(userClient.getRealName(), userEncodedRealName);
        Assert.assertTrue(userEncodedPassword.isEmpty());
        Assert.assertEquals(userClient.getRole().toInteger(), userEncodedRole);
    }

    private Task createRandomTaskWithUserClient(UserClient userClient) throws java.text.ParseException {
        final Task task = DaoHelper.createRandomTask();
        task.addUserClient(userClient);
        return task;
    }

}
