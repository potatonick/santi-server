/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

import com.gitlab.potatonick.santiserver.dao.TaskDao;
import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import com.gitlab.potatonick.santiserver.helpers.DaoHelper;
import com.gitlab.potatonick.santiserver.helpers.JSONHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ControllerTest {

    private static Task TASK;

    static {
        try {
            TASK = DaoHelper.createRandomTask();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }

    private static final UserClient USER_CLIENT = DaoHelper.createRandomClient();
    private static final String INCORRECT_PASSWORD = "incorrect password";

    private final JSONParser parser = new JSONParser();
    private final Controller controller = new Controller();

    public ControllerTest() { }

    @BeforeClass
    public static void createCleanSession() {
        // WARNING: tables will be erased when this test runs
        HibernateFactory.close();
        HibernateFactory.getInstance(HibernateFactory.Mode.CREATE_DROP);

        // Save task and user in database
        final TaskDao taskDao = new TaskDao();
        TASK.addUserClient(USER_CLIENT);
        Assert.assertTrue(taskDao.insertTask(TASK));
    }

    @AfterClass
    public static void closeSessionFactory() {
        HibernateFactory.close();
    }

    @Test
    public void shouldIdentifyUserClient() {
        final String userName = USER_CLIENT.getUserName();
        final String password = USER_CLIENT.getPassword();
        final String request = generateRequest(userName, password, "check-credentials");

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(isOkResponse(response));
    }

    @Test
    public void shouldNotIdentifyUserClientWithIncorrectCredentials() {
        final String userName = USER_CLIENT.getUserName();
        final String password = INCORRECT_PASSWORD;
        final String request = generateRequest(userName, password, "check-credentials");

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        final String error = getErrorCode(response);
        Assert.assertEquals(ErrorCode.UNAUTHORIZED.toString(), error);
    }

    @Test
    public void shouldCreateNewTaskWhenAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = USER_CLIENT.getPassword();
        final String request = generateCreateTaskRequest(userName, password);

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(isOkResponse(response));
    }

    @Test
    public void shouldNotCreateNewTaskWhenNotAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = INCORRECT_PASSWORD;
        final String request = generateCreateTaskRequest(userName, password);

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        final String error = getErrorCode(response);
        Assert.assertEquals(ErrorCode.UNAUTHORIZED.toString(), error);
    }

    @Test
    public void shouldGetAllTasksWhenAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = USER_CLIENT.getPassword();
        final String request = generateRequest(userName, password, "get-tasks");

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(isOkResponse(response));
    }

    @Test
    public void shouldNotGetTasksWhenNotAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = INCORRECT_PASSWORD;
        final String request = generateRequest(userName, password, "get-tasks");

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        final String error = getErrorCode(response);
        Assert.assertEquals(ErrorCode.UNAUTHORIZED.toString(), error);
    }

    @Test
    public void shouldUpdateTaskWhenAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = USER_CLIENT.getPassword();
        final String request = generateUpdateTaskRequest(userName, password);

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(isOkResponse(response));
    }

    @Test
    public void shouldNotUpdateTaskWhenAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = INCORRECT_PASSWORD;
        final String request = generateUpdateTaskRequest(userName, password);

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        final String error = getErrorCode(response);
        Assert.assertEquals(ErrorCode.UNAUTHORIZED.toString(), error);
    }

    @Test
    public void shouldDeleteTaskWhenAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = USER_CLIENT.getPassword();
        final String request = generateDeleteTaskRequest(userName, password);

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(isOkResponse(response));
    }

    @Test
    public void shouldNotDeleteTaskWhenNotAuthorized() {
        final String userName = USER_CLIENT.getUserName();
        final String password = INCORRECT_PASSWORD;
        final String request = generateDeleteTaskRequest(userName, password);

        final String response = controller.executeRequestAndGetResponse(request);
        Assert.assertNotNull(response);
        final String error = getErrorCode(response);
        Assert.assertEquals(ErrorCode.UNAUTHORIZED.toString(), error);
    }

    private String generateRequest(String user, String password, String command) {
        final JSONObject request = new JSONObject();
        request.put("user", user);
        request.put("password", password);
        request.put("command", command);
        return request.toJSONString();
    }

    private String generateCreateTaskRequest(String user, String password) {
        final JSONObject request = new JSONObject();
        request.put("user", user);
        request.put("password", password);
        request.put("command", "create-task");

        // Task
        final JSONObject task = new JSONObject();
        request.put("data", task);
        task.put("title", "Task title");
        task.put("description", "Task description");
        task.put("state", Task.State.IN_PROGRESS.toInteger());
        task.put("priority", Task.Priority.URGENT.toInteger());
        task.put("start-date", "12-05-2019");
        task.put("end-date", "13-05-2019");

        // Assign a persisted user
        final JSONArray users = new JSONArray();
        users.add(USER_CLIENT.getId());
        task.put("users", users);

        return request.toJSONString();
    }

    private String generateUpdateTaskRequest(String user, String password) {
        final JSONObject request = new JSONObject();
        request.put("user", user);
        request.put("password", password);
        request.put("command", "update-task");

        // Task
        final JSONObject task = new JSONObject();
        request.put("data", task);
        task.put("id", TASK.getId());
        task.put("title", "Task title");
        task.put("description", "Task description");
        task.put("state", Task.State.IN_PROGRESS.toInteger());
        task.put("priority", Task.Priority.URGENT.toInteger());
        task.put("start-date", "12-05-2019");
        task.put("end-date", "13-05-2019");

        // Assign a persisted user
        final JSONArray users = new JSONArray();
        users.add(USER_CLIENT.getId());
        task.put("users", users);

        return request.toJSONString();
    }

    private String generateDeleteTaskRequest(String user, String password) {
        final JSONObject request = new JSONObject();
        request.put("user", user);
        request.put("password", password);
        request.put("command", "remove-task");
        request.put("data", TASK.getId());
        return request.toJSONString();
    }

    private boolean isOkResponse(String response) {
        try {
            JSONObject res = (JSONObject) parser.parse(response);
            final String status = JSONHelper.getValue(res, "status", String.class);
            return status.equals("ok");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    private String getErrorCode(String response) {
        try {
            JSONObject res = (JSONObject) parser.parse(response);
            return JSONHelper.getValue(res, "error", String.class);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return ErrorCode.NONE.toString();
    }

}
