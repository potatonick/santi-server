/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of santi-server.
 *
 * santi-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * santi-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with santi-server. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.santiserver.controller;

import com.gitlab.potatonick.santiserver.dao.UserClientDao;
import com.gitlab.potatonick.santiserver.entity.Task;
import com.gitlab.potatonick.santiserver.entity.UserClient;
import com.gitlab.potatonick.santiserver.helpers.DaoHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RequestDecoderTest {

    private final JSONObject requestModel = new JSONObject();
    private RequestDecoder requestDecoder;
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    private final String originalCommand = "check-credentials";
    private final String originalUser = "pepito";
    private final String originalPassword = "1234";
    private static final Task ORIGINAL_TASK = new Task();

    public RequestDecoderTest() {
        requestModel.put("command", originalCommand);
        requestModel.put("user", originalUser);
        requestModel.put("password", originalPassword);
    }

    @BeforeClass
    public static void prepareDatabase() throws ParseException {
        // WARNING: tables will be erased when this test runs
        HibernateFactory.close();
        HibernateFactory.getInstance(HibernateFactory.Mode.CREATE_DROP);

        // Create a mock task
        ORIGINAL_TASK.setTitle("Eat vegetables");
        ORIGINAL_TASK.setDescription("They're healthy");
        ORIGINAL_TASK.setState(Task.State.IN_PROGRESS);
        ORIGINAL_TASK.setPriority(Task.Priority.URGENT);
        ORIGINAL_TASK.setPlannedStartDate(DATE_FORMAT.parse("24-04-2005"));
        ORIGINAL_TASK.setPlannedEndDate(DATE_FORMAT.parse("04-11-2008"));

        // Add random users to the task and save them into the DB
        final UserClientDao userClientDao = new UserClientDao();
        for (int i = 0; i < 3; i++) {
            final UserClient randomUserClient = DaoHelper.createRandomClient();
            Assert.assertTrue(userClientDao.insertUserClient(randomUserClient));

            // Get the generated UserClient directly from the database (we need the real ID)
            final UserClient findUserClient = userClientDao.findUserClientByUsername(randomUserClient.getUserName());
            ORIGINAL_TASK.addUserClient(findUserClient);
        }
    }

    @Test
    public void shouldGetCommand() {
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final String decodedCommand = requestDecoder.getCommand();

        Assert.assertNotNull(decodedCommand);
        Assert.assertEquals(originalCommand, decodedCommand);
    }

    @Test
    public void shouldGetEmptyCommandWhenNoCommandGiven() {
        requestModel.remove("command");
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final String decodedCommand = requestDecoder.getCommand();

        Assert.assertNotNull(decodedCommand);
        Assert.assertTrue(decodedCommand.isEmpty());
    }

    @Test
    public void shouldGetUser() {
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final String decodedUser = requestDecoder.getUser();

        Assert.assertNotNull(decodedUser);
        Assert.assertEquals(originalUser, decodedUser);
    }

    @Test
    public void shouldGetEmptyUserWhenNoUserGiven() {
        requestModel.remove("user");
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final String decodedUser = requestDecoder.getUser();

        Assert.assertNotNull(decodedUser);
        Assert.assertTrue(decodedUser.isEmpty());
    }

    @Test
    public void shouldGetPassword() {
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final String decodedPassword = requestDecoder.getPassword();

        Assert.assertNotNull(decodedPassword);
        Assert.assertEquals(originalPassword, decodedPassword);
    }

    @Test
    public void shouldGetEmptyPasswordWhenNoPasswordGiven() {
        requestModel.remove("password");
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final String decodedPassword = requestDecoder.getPassword();

        Assert.assertNotNull(decodedPassword);
        Assert.assertTrue(decodedPassword.isEmpty());
    }

    @Test
    public void shouldDecodeTask() {
        addMockTaskToMockRequest();
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final Task decodedTask = requestDecoder.getTask();

        Assert.assertNotNull(decodedTask);
        Assert.assertEquals(ORIGINAL_TASK, decodedTask);
    }

    @Test
    public void shouldReturnNullWhenNoTaskGiven() {
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final Task decodedTask = requestDecoder.getTask();
        Assert.assertNull(decodedTask);
    }

    @Test
    public void shouldDecodeID() {
        final Long mockID = 24L;
        requestModel.put("data", mockID);
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final Long decodedID = requestDecoder.getID();

        Assert.assertNotNull(decodedID);
        Assert.assertEquals(mockID, decodedID);
    }

    @Test
    public void shouldReturnNullWhenNoIDGiven() {
        final String mockRequest = requestModel.toJSONString();
        requestDecoder = new RequestDecoder(mockRequest);
        final Long decodedID = requestDecoder.getID();

        Assert.assertNull(decodedID);
    }

    private void addMockTaskToMockRequest() {
        JSONObject mockTask = new JSONObject();
        requestModel.put("data", mockTask);

        mockTask.put("title", ORIGINAL_TASK.getTitle());
        mockTask.put("description", ORIGINAL_TASK.getDescription());
        mockTask.put("state", ORIGINAL_TASK.getState().toInteger());
        mockTask.put("priority", ORIGINAL_TASK.getPriority().toInteger());
        mockTask.put("start-date", DATE_FORMAT.format(ORIGINAL_TASK.getPlannedStartDate()));
        mockTask.put("end-date", DATE_FORMAT.format(ORIGINAL_TASK.getPlannedEndDate()));

        JSONArray mockUsers = new JSONArray();
        mockTask.put("users", mockUsers);
        for (final UserClient userClient : ORIGINAL_TASK.getUserClients()) {
            mockUsers.add(userClient.getId());
        }
    }

}
